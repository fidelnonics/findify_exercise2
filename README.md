# findify_exercise2

# Implementing slider, following the suggested design. Some limitations:

* Use only CSS for styling, no images
* You can use either your favorite frontend framework (React, Angular, Vue,...) or plain JavaScript
* You can't use any pre-built slider
* The slider should work properly with updating the values in the tooltip

The design: http://prntscr.com/mwsxez

# The running design can be found [here](https://fidelnonics.gitlab.io/findify_exercise2/)