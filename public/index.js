$.fn.WBslider = function() {
	return this.each(function() {
		var $_this = $(this),
			$_date = $('input', $_this),
			$_title = $('.start_amount', $_this),
			thumbwidth = 50, // set this to the pixel width of the thumb
			max_amount = 1000000;

		// set range max to current year
		$_date.attr('max', max_amount);
		//$('.end_amount', $_this).text( max_amount );
		$_date.val(max_amount); // -10 years, just because...

		$_date.on('input change keyup', function() {
			var $_this = $(this),
				val = parseInt($_date.val(), 10);

			if (val < 1) {
				val = '0';
			}
			if (val === '') { // Stop IE8 displaying NaN
				val = 0;
			}

			$_title.text( val );

			var pos = (val - $_date.attr('min'))/($_date.attr('max') - $_date.attr('min'));

			// position the title with the thumb
			var thumbCorrect = thumbwidth * (pos - 0.5) * -1,
				titlepos = Math.round( ( pos * $_date.width() ) - thumbwidth/4 + thumbCorrect );

			$_title.css({'left': titlepos});

			// show "progress" on the track
			pos = Math.round( pos * 99 ); // to hide stuff behide the thumb
			var grad = 'linear-gradient(90deg, #5f80b3 ' + pos + '%,#314869 ' + (pos+1) + '%)';
			$_date.css({'background': grad});

		}).on('focus', function() {
			if ( isNaN( $(this).val() ) ) {
				$(this).val(0);
			}
		}).trigger('change');
		
		$(window).on('resize', function() {
			$_date.trigger('change');
		});
	});
};

$(function() {

	$('.slider').WBslider();

});